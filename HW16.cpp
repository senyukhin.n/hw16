﻿// HW16.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <time.h>

int main()
{
	const int N = 5;
	int array[N][N];
	for (int i=0;i<N;i++)
	{
		for (int j = 0; j<N;j++)
		{
			array[i][j] = i + j;
		}
	}
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			std::cout << array[i][j];
		}
		std::cout << '\n';
	}
	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);
	int x = buf.tm_mday % N; // остаток от деления даты на размер
	int sum = 0;
	for (int j=0; j<N;j++)
	{
		sum = sum + array[x][j];
	}
	std::cout << sum;
}


